# Orvet

A diy weather (temperature/humidity) station that push its data somewhere (currently thingspeak).

Based on ESP8266.

## Code
Code is 


## Electronic & PCB 
### Schema and pcb
 They can be found in [kicad](/kicad). They have been created with **kicad 5** and require updated:
 *  [kicad-symbols](https://github.com/KiCad/kicad-symbols)
 *  [kicad-footprints](https://github.com/KiCad/kicad-footprints)
 *  [Crafting Labs kicad library](https://gitlab.com/avernois/kicad-lib)

Note: That pcb has not been manufactured yet, thus not tested. There might be

### BOM
 * U1: HT7333 (low drop regulator) in TO-92 package
 * U2: ESP12-E (or ESP12-F) on a breakout board (with pin headers)
 * J1: 1x2 pin header
 * C1: 33uF capacitor
 * U3: si7021 i2c temperature/humidity sensor on a breakout board (+pin header)
 * SW1: momentary push button (optional)
 
## Have a question? Find an issue?

Please open an [issue](https://gitlab.com/avernois/kicad-lib/issues) or contact me on twitter [@avernois](https://twitter.com/avernois).

## Contribution

Contribution are very welcomed.

Note: I have strong opinion on what that project is about, so if you develop a feature and want it to be merged here, please come talk to me (see above) before putting too much effort in it :)

## Licence
Current project is under MIT License. See (/LICENSE) for details.